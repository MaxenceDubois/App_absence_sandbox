<?php
include "database_login.php";
include "../models/validator.php";

$username=strip_tags($_POST['username']);
$password=strip_tags($_POST['password']);

if (!empty($username) && !empty($password)) {
  echo validator($username,$password,$handle);
} else {
  echo "Veuillez renseigner tous les champs";
}
?>
