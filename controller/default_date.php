<?php

// If no date are specified, enter today's date
if (empty($_SESSION['day']) && empty($_SESSION['month']) && empty($_SESSION['year'])) {
  $_SESSION['day']=date('d');
  $_SESSION['month']=date('m');
  $_SESSION['year']=date('Y');
}

 ?>
