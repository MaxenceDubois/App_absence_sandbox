<?php
include "view/header.php"
 ?>

<body>
 <div class="row">
   <div class="col s12 m6 offset-m3">
     <div class="card">
       <form action="controller/check_login.php" method="post">
         <div class="card-content">
           <h1 class="card-title">Connexion</h1>
           <!--Print a formular for login access-->
             <div class="row">
               <div class="col s10 offset-s1">
                 <input type="text" name="username" placeholder="Entrez votre pseudo" required>
               </div>
             </div>
             <div class="row">
               <div class="col s10 offset-s1">
                 <input type="password" name="password" placeholder="Entrez votre mot de passe" required>
               </div>
             </div>
         </div>
         <div class="card-action">
           <div class="row">
             <div class="col s10 offset-s1 center">
               <button class="btn waves-effect waves-light" type="submit" name="action">Connexion
                 <i class="material-icons right">send</i>
               </button>
             </div>
           </div>
         </div>
     </form>
     </div>
   </div>
 </div>
</html>
