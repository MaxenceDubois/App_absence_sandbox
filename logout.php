<?php

include ('../app_absence/view/header.php');

// Overwrite the session array
$_SESSION = array();

// Destroy the session
session_destroy();

// Return to login page
header('Location: index.php');

?>
