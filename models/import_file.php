<?php

$query="SELECT * FROM missed WHERE `date`=" . $_SESSION['day'].$_SESSION['month'].$_SESSION['year'] . " AND id_person=" . $student;
$result=mysqli_query($handle,$query);

if (!empty($student)) {
  // Testons si le fichier a bien été envoyé et s'il n'y a pas d'erreur
  if (isset($_FILES['justification']) && $_FILES['justification']['error'] == 0){
    // Testons si le fichier n'est pas trop gros
    if ($_FILES['justification']['size'] <= 10000000){

      // If size is OK, move the uploaded file
      $resultat = move_uploaded_file($_FILES['justification']['tmp_name'], '../uploads/' . basename($_FILES['justification']['name']));

      // Keep the emplacement in dtb
      $info= basename($_FILES['justification']['name']);

      // If there is already a missed, update the line with the justification
      if (mysqli_num_rows($result) > 0) {
        $query="UPDATE missed SET justification='" . $info . "' WHERE id_person=" . $student . " AND `date`=" . $_SESSION['day'].$_SESSION['month'].$_SESSION['year'];
        $result=mysqli_query($handle,$query);
        echo "test";

        // If not, add a new line
      } else {
        $query="INSERT INTO missed (`date`, `id_person`, `missed_morning`, `justification`) VALUES (" . $_SESSION['day'].$_SESSION['month'].$_SESSION['year'] . ", " . $student . ", 1, '" . $info . "')";
        echo $query;
        $result=mysqli_query($handle,$query);
      }

      // Check if it has worked
      if (mysqli_affected_rows($handle)>0) {
        header("Location: ../". $_SESSION['access_level'] ."_page.php");
        die;
      } else {
        echo "L'envoi du fichier a échoué, veuillez recommencer.";
      }
    } else {
      echo "Fichier tros gros";
    }
  } else {
    echo "Fichier non reçu ou incompatible";
  }
} else {
  echo "Vous devez spécifier un étudiant";
}

?>
