<!-- Start the dropdown effect -->
<script type="text/javascript">
$(document).ready(function() {
  $('select').material_select();
});
</script>

<!-- Print the date selector -->
<div class="row">
  <div class="col s12 m6 offset-m3">
    <div class="card blue-grey darken-1">
      <div class="card-content white-text">
        <span class="card-title">Choisissez le jour</span>
        <form class="row offset-m1" action="controller/change_date.php" method="post">
          <select class="col s3" name="day">
            <option value="<?php echo $_SESSION['day'] ?>"><?php echo $_SESSION['day'] ?></option>
            <?php for ($day = 1; $day <= 31; $day++) { ?>
              <option value="<?php echo strlen($day)==1 ? '0'.$day : $day; ?>"><?php echo strlen($day)==1 ? '0'.$day : $day; ?></option>
              <?php } ?>
            </select>
            <select class="col s3" name="month">
              <option value="<?php echo $_SESSION['month'] ?>"><?php echo $_SESSION['month'] ?></option>
              <?php for ($month = 1; $month <= 12; $month++) { ?>
                <option value="<?php echo strlen($month)==1 ? '0'.$month : $month; ?>"><?php echo strlen($month)==1 ? '0'.$month : $month; ?></option>
                <?php } ?>
              </select>
              <select class="col s3" name="year">
                <option value="<?php echo $_SESSION['year'] ?>"><?php echo $_SESSION['year'] ?></option>
                <?php for ($year=date('Y'); $year > date('Y')-3; $year--) { ?>
                  <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                  <?php } ?>
                </select>
                <button class="btn-flat white-text waves-effect waves-light col s2 offset-s1" type="submit" name="action">Aller</button>
              </form>
            </div>
          </div>
        </div>
      </div>
