<div class="row">
        <div class="col s12 m6 offset-m3">
          <div class="card">
            <div class="card-content">
              <span class="card-title">Envoyer un justificatif</span>
<form enctype="multipart/form-data" action="controller/import_file.php" method="post">
  <div class="file-field input-field">
    <div class="btn">
      <span>Fichier (max:10Mb)</span>
      <input type="hidden" name="MAX_FILE_SIZE" value="10000000">
      <input type="file" name="justification" id="justification">
    </div>
    <div class="file-path-wrapper">
      <input class="file-path validate" type="text">
    </div>
  </div>
  <div class="input-field col s12">
    <select name="id_student">
      <?php
      $query="SELECT * FROM popschoolers WHERE id_promotion=" . $_SESSION['id_promotion'];
      $result = mysqli_query($handle,$query);
      while($line = mysqli_fetch_array($result)) {
        echo "<option value='" . $line["id"] . "'>" . $line["first_name"] . " " . $line["last_name"] . "</option>";
      }
      ?>
    </select>
  </div>
  <div class="row center">
    <button class="btn waves-effect waves-light" type="submit" name="action">Envoyer
      <i class="material-icons right">send</i>
    </button>
  </div>
</form>
</div>
          </div>
        </div>
      </div>
