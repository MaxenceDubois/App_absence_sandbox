<?php

function list_popschoolers($handle) {
  $query="SELECT * FROM popschoolers WHERE id_promotion=" . $_SESSION["id_promotion"];
  $result=mysqli_query($handle,$query);

  // Print each student name
  while ($line = mysqli_fetch_array($result)) {
    echo "<div class='row'>";
      echo "<p class='col s6'>" . $line['first_name'] . " " . $line['last_name'] . "</p>";
      echo "<input name='student' type='hidden' value'". $line['id'] . "'>";
      echo "<div class='right'>";
      $query2="SELECT * FROM missed WHERE `date`=" . $_SESSION['day'].$_SESSION['month'].$_SESSION['year'] . " AND `id_person`=" . $line['id'];
      $result2=mysqli_query($handle,$query2);

      // Check if the student was present or not. If present, check 'présent'
      if ($result2) {
        $line2 = mysqli_fetch_array($result2);

        // If not present, check 'absent'
        if ($line2['missed_morning']==1) {
          $check_pres="";
          $ckeck_abs="checked='checked'";

          // If there is a justification, give a link
          if (!empty($line2['justification'])) {
            $justif="<a href='uploads/" . $line2['justification'] . "'>Voir</a> ";
          }
        } else {
          $check_pres="checked='checked'";
          $ckeck_abs="";
          $justif="";
        }
      } else {
        $check_pres="checked='checked'";
        $ckeck_abs="";
        $justif="";
      }
      echo $justif;
      echo "<input name='group" . $line['id'] . "' type='radio' value=0 id='test" . $line['id'] . "1' " . $check_pres . "><label for='test" . $line['id'] . "1'>Présent </label>";
      echo "<input name='group" . $line['id'] . "' type='radio' value=1 id='test" . $line['id'] . "2' " . $ckeck_abs . "><label for='test" . $line['id'] . "2'>Absent </label>";
      echo "</div>";
    echo "</div>";
  }
}
 ?>
