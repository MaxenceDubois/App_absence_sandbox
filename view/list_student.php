<div class="row">
  <div class="col s12 m6 offset-m3">
    <div class="card">
      <div class="card-content">

        <!-- Select with promotion's id a promotion to print -->
        <form action="controller/select_promotion.php" method="post">
          <div class="row">
            <select class="col s9" name="id_promotion">

              <!-- List all available promotions -->
              <?php list_promotions($handle); ?>
            </select>
            <button class="col s2 offset-s1 btn waves-effect waves-light" type="submit">Choisir</button>
          </div>
        </form>

        <!-- List all the student from that promotion -->
        <form action="controller/add_missed.php" method="post">
          <div class="row">

            <!-- List all students from the selected promotion -->
            <?php list_popschoolers($handle); ?>
          </div>
          <button class="btn waves-effect waves-light" type="submit">Envoyer</button>
        </form>
      </div>
    </div>
  </div>
</div>
