<?php include "header.php" ?>

<nav>
  <div class="nav-wrapper teal lightn-2">
    <div class="container">
      <a href="" class="brand-logo">Pop School app-absence</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
  	     <a href="logout.php"class="waves-effect waves-light btn-flat white-text">Déconnexion</a>
      </ul>
	</div>
  </div>
</nav>
